<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title>Sistema Escolar</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.3.min.js"></script>
       
    </head>

    <body>
        <?php
            include "conecta_db.php";
            session_start();
            $usuario_logado = $_SESSION['user'];
            $senha_usuario = $_SESSION['pass'];

            if($usuario_logado == "" || $senha_usuario == ""){
                print "Você não está logado! <a href='index.php'>Clique Aqui</a> para efetuar o login!";
                print$usuario_logado . "<br>" . $senha_usuario;
            }
           else {
        ?>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <h2>PAINEL DE CONTROLE - SISTEMA DE ALUNOS</h2>
                    <p>Você está logado como: <?php print $usuario_logado; ?></p>
                    <p>Escolha uma opção</p>
                </div>
            </div>
            <div class="botoes">
                <div class="botao-dash" id="cad-aluno">
                    <h4>CADASTRAR ALUNOS</h4>
                </div>
                <div class="botao-dash" id="chama-lista">
                    <h4>EDITAR ALUNOS</h4>
                </div>
                <a href="index.php" class="link-sair">
                <div class="botao-dash">
                    <h4>SAIR</h4>
                </div>
                </a>
            </div>

            <div class="row" id="ficha-cadastro" style="display: none;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <h2>Ficha de Cadastro</h2>
                    <form method="post" action="inclui_aluno.php">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" class="form-control" name="nome" placeholder="Informe o nome do aluno" required>
                            <p id="p-nome" style="color: #f00; display: none;">Você não informou o nome</p>
                        </div>
                        <div class="form-group">
                            <label for="datanasc">Data de Nascimento</label>
                            <input type="date" id="datanasc" class="form-control" name="datanasc" placeholder="Informe a data de nascimento" required>
                            <p id="p-datanasc" style="color: #f00; display: none;">Você não informou a data de nascimento</p>
                        </div>
                        <div class="form-group">
                            <label for="serie">Série</label>
                            <input type="text" id="serie" class="form-control" name="serie" placeholder="Informe a série" required>
                            <p id="p-serie" style="color: #f00; display: none;">Você não informou a série</p>
                        </div>
                        <div class="form-group">
                            <label for="escola">Escola</label>
                            <input type="text" id="escola" class="form-control" name="escola" placeholder="Informe a escola" required>
                            <p id="p-escola" style="color: #f00; display: none;">Você não informou a escola</p>
                        </div>
                        <div class="form-group">
                            <label for="sexo">Sexo</label>
                            <select class="form-control" name="sexo">
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-success">CADASTRAR</button>
                    </form>
                </div>
                <div class="col-sm-2"></div>
            </div>
            <div class="row" id="lista-alunos" style="display: none;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <h2>Listagem de Alunos</h2>
                    <?php
                      $sql = mysqli_query($connect, "SELECT `ID`, `nome`, `data_nasc`, `serie`, `escola`, `sexo` FROM tb_aluno");
                      $row = mysqli_num_rows($sql);

                     
                      


                    ?>
                    <table class="table table-responsive">
                        <thead class="thead-dark">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">NOME</th>
                                    <th scope="col">DATA DE NASCIMENTO</th>
                                    <th scope="col">SÉRIE</th>
                                    <th scope="col">ESCOLA</th>
                                    <th scope="col">SEXO</th>
                                    <th scope="col" colspan="2">AÇÕES</th>
                                </tr>
                        </thead>
                        <tbody>
                                    <?php

                            while($row = mysqli_fetch_array($sql)): ?>
                                <tr>
                                    <td><?php print $row['ID']; ?></td>
                                    <td><?php print $row['nome']; ?></td>
                                    <td><?php print $row['data_nasc']; ?></td>
                                    <td><?php print $row['serie']; ?></td>
                                    <td><?php print $row['escola']; ?></td>
                                    <td><?php print $row['sexo']; ?></td>
                                    <td><a href="update.php?update_id=<?php echo $row['ID']; ?>">
                            Editar
                            </a></td>
                                    <td><a href="delete.php?delete_id=<?php echo $row['ID']; ?>">
                            Excluir
                            </a></td>
                                </tr>
                                <?php endwhile; ?>
	                        </tbody>

                        <!--
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Victor Wurthmann</td>
                                <td>28/02/1993</td>
                                <td>8 série</td>
                                <td>Reynaldo Kuntz Busch</td>
                                <td>Masculino</td>
                                <td><a href="#">Editar</a></td>
                                <td><a href="#">Excluir</a></td>
                            </tr>

                        </tbody> -->
                    </table>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
        <?php } ?>
        
        
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        //valida ficha de cadastro
        $(document).ready(function(){
            $("#nome").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-nome").css("display","block");
                }
            else{
                $("#p-nome").css("display","none");
            }
            });
            $("#datanasc").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-datanasc").css("display","block");
                }
            else{
                $("#p-datanasc").css("display","none");
            }
            });
            $("#serie").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-serie").css("display","block");
                }
            else{
                $("#p-serie").css("display","none");
            }
            });
            $("#escola").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-escola").css("display","block");
                }
            else{
                $("#p-escola").css("display","none");
            }
            });
        })

               
               
               //funcionalidade botoes dashboard
                $("#cad-aluno").click(function(){
                    $("#ficha-cadastro").css("display","flex");
                    $("#lista-alunos").css("display", "none");
                });
                $("#chama-lista").click(function(){
                    $("#ficha-cadastro").css("display","none");
                    $("#lista-alunos").css("display", "flex");
                });

               

            
        </script>
    </body>
    
</html>