<?php include "conecta_db.php";
if(isset($_POST['update'])){
  $nome = $_POST['nome'];
  $data_nasc = $_POST['datanasc'];
  $serie = $_POST['serie'];
  $escola = $_POST['escola'];
  $sexo = $_POST['sexo'];
  $id = $_POST['edit_id'];
  $query = mysqli_query($connect, "UPDATE tb_aluno SET nome = '$nome' ,data_nasc = '$data_nasc', serie = '$serie', escola = '$escola', sexo = '$sexo' WHERE id = '$id'");
  if ($query) {
    header('location:dashboard.php');
  }else{
    print "<script>alert('Não foi possível atualizar')</script>";
  }
}

 ?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title>Sistema Escolar</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>

    <body>
        <div class="container">
           
            <?php

                if(isset($_GET['update_id'])); 
                    @$id = $_GET['update_id']; 
                    $query = mysqli_query($connect, "SELECT * FROM tb_aluno WHERE id = '$id' ");
                    $row = mysqli_fetch_array($query);
                    $nome = $row['nome'];
                    $data_nasc = $row['data_nasc'];
                    $serie = $row['serie'];
                    $escola = $row['escola'];
                    $sexo = $row['sexo'];
                ?>

            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <h2>Ficha de Cadastro</h2>
                    <form method="post" action="update.php">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" class="form-control" name="nome" placeholder="Informe o nome do aluno" value="<?php print $nome; ?>" required>
                            <p id="p-nome" style="color: #f00; display: none;">Você não informou o nome</p>
                        </div>
                        <div class="form-group">
                            <label for="datanasc">Data de Nascimento</label>
                            <input type="date" id="datanasc" class="form-control" name="datanasc" placeholder="Informe a data de nascimento" value="<?php print $data_nasc; ?>" required>
                            <p id="p-datanasc" style="color: #f00; display: none;">Você não informou a data de nascimento</p>
                        </div>
                        <div class="form-group">
                            <label for="serie">Série</label>
                            <input type="text" id="serie" class="form-control" name="serie" placeholder="Informe a série" value="<?php print $serie; ?>" required>
                            <p id="p-serie" style="color: #f00; display: none;">Você não informou a série</p>
                        </div>
                        <div class="form-group">
                            <label for="escola">Escola</label>
                            <input type="text" id="escola" class="form-control" name="escola" placeholder="Informe a escola" value="<?php print $escola; ?>" required>
                            <p id="p-escola" style="color: #f00; display: none;">Você não informou a escola</p>
                        </div>
                        <div class="form-group">
                            <label for="sexo">Sexo</label>
                            <select class="form-control" name="sexo">
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            </select>
                        </div>
                        <input type="hidden" name="edit_id" value="<?php print $id; ?>">
                        <button type="submit" name="update" class="btn btn-success">ATUALIZAR</button>
                    </form>
                </div>
                <div class="col-sm-2"></div>
            </div>
           
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script>
            //valida ficha de cadastro
        $(document).ready(function(){
            $("#nome").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-nome").css("display","block");
                }
            else{
                $("#p-nome").css("display","none");
            }
            });
            $("#datanasc").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-datanasc").css("display","block");
                }
            else{
                $("#p-datanasc").css("display","none");
            }
            });
            $("#serie").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-serie").css("display","block");
                }
            else{
                $("#p-serie").css("display","none");
            }
            });
            $("#escola").blur(function(){
            if($(this).val() == "")
                {
                    $("#p-escola").css("display","block");
                }
            else{
                $("#p-escola").css("display","none");
            }
            });
        })
        </script>
    </body>
    
</html>